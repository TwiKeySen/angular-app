import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { User } from '../models/user.interface';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { extractData } from '../tools/treat-data';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  public getUser(id: number) : Observable<User>
  {
    return this.httpClient.get<User>(`${environment.apiUrl}/users/${id}`).pipe(
      map(data => { return data })
    );
  }

  public getUsers() : Observable <User[]> 
  {
    return this.httpClient.get<User[]>(`${environment.apiUrl}/users`).pipe(
      map(data => { return data })
    );
  }

  public createUser(user: User)
  {
    return this.httpClient.post(`${environment.apiUrl}/users/register`, user).pipe(
      map(data => { return data })
    );;
  }

  public updateUser(id: number, user: User)
  {
    return this.httpClient.patch(`${environment.apiUrl}/users/${id}`, user).pipe(
      map(data => { return data })
    );
  }

  public deleteUser(id: number)
  {
    return this.httpClient.delete(`${environment.apiUrl}/users/${id}`);
  }
}
