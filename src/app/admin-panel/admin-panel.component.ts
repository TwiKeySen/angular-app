import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../_services/user.service'; 
import { User } from '../models/user.interface';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {

  public newUserForm: FormGroup;
  public editUserForm: FormGroup; 
  public newUserFormIsSubmitted: boolean = false;
  public editUserFormIsSubmitted: boolean = false;
  public isLoggedIn = false;
  public userToDelete: number;

  public users: User[];

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private tokenStorageService: TokenStorageService,
    private router: Router,
  ) { }

  ngOnInit(): void {

    this.newUserForm = this.formBuilder.group({
      firstname: [''],
      lastname: [''],
      email: ['', Validators.required],
      password: ['', Validators.required],
      password_repeat: ['', Validators.required],
    })

    this.editUserForm = this.formBuilder.group({
      id: [''],
      firstname: [''],
      lastname: [''],
      email: ['', Validators.required],
    })

    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      this.getDatas();
    }
  }


  getDatas() {
    this.userService.getUsers().subscribe(
      res => {
        this.users = res;
      }
    )
  }

  getIdOfUserToDelete(id: number) {
    $('#modalDeleteUser').modal('show');
    this.userToDelete = id;
  }

  fillEditForm(user: User) {
    this.editUserForm.get('id').setValue(user.id);
    this.editUserForm.get('firstname').setValue(user.firstname);
    this.editUserForm.get('lastname').setValue(user.lastname);
    this.editUserForm.get('email').setValue(user.email);    

    $('#modalEditUser').modal('show');
  }

  updateUser() {
    this.editUserFormIsSubmitted = true;

    if (this.editUserForm.invalid)
    {
      return;
    }
  
    this.userService.updateUser(this.editUserForm.get('id').value, this.editUserForm.value).subscribe(
      res => {
        console.log(res);
        if (res) {
          this.getDatas();
          $('#modalEditUser').modal('hide');
        }
      }
    )
  }

  get editFormControls() { return this.editUserForm.controls }

  get f() { return this.newUserForm.controls }

  onSubmit() {
    this.newUserFormIsSubmitted = true;

    if (this.newUserForm.invalid)
    {
      return;
    }

    this.userService.createUser(this.newUserForm.value).subscribe(
      res => {
        if (res) {
          this.getDatas();
          $("#modalNewUser").modal('hide');
        }
      }
    )

  }

  delete() {
    if(this.userToDelete)
    {
      this.userService.deleteUser(this.userToDelete).subscribe(
        res => {
          this.getDatas();
          $('#modalDeleteUser').modal('hide');
        }
      )
    }
  }

  logout() {
    this.tokenStorageService.signOut();
    this.router.navigate(['/']);
  }

}
