import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  public loginForm: FormGroup;
  public isSubmitted: boolean = false;

  isLoggedIn = false;
  isLoginFailed = false;

  /**
   * When the component is constructed, we need to lookup for formBuilder & router 
   * if we want to use them inside the class
   * 
   * @param formBuilder 
   * @param router 
   */
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
  ) { }

  /**
   * When the component is initialized,
   * we create a form group with formBuilder
   * We force email and password to be required
   * 
   * Of course we can enable more than just one Validator
   */
  ngOnInit(): void {

    this.loginForm = this.formBuilder.group({
      email: [ '', Validators.required],
      password: ['', Validators.required],
    })

  }

  /**
   * This method will return the controls from the formBuilder
   */
  get f() { return this.loginForm.controls }

  /**
   * In this method, we will change the property isSubmitted to true,
   * Then we will check if the loginForm is valid,
   * If not, we return and do not proceed to the validation of the form
   */
  onSubmit() {
    this.isSubmitted = true;

    if (this.loginForm.invalid) 
    {
      return;
    }

    this.authService.login(this.loginForm.value).subscribe(
      res => {
        this.tokenStorage.saveToken(res.user.access_token);
        this.tokenStorage.saveUser(res.user);

        this.isLoginFailed = false;
        this.isLoggedIn = true;

        if (res.user.active) {
          this.router.navigate(['/admin-panel']);
        }
      }
    )
  }
}
