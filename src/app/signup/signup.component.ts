import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public signupForm: FormGroup;
  public isSubmitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
  ) { }

  ngOnInit(): void {

    this.signupForm = this.formBuilder.group({
      firstname: [''],
      lastname: [''],
      email: ['', Validators.required],
      password: ['', Validators.required],
      password_repeat: ['', Validators.required],
    });

  }

  get f() { return this.signupForm.controls }

  onSubmit() {
    this.isSubmitted = true;

    if (this.signupForm.invalid)
    {
      return;
    }

    this.userService.createUser(this.signupForm.value).subscribe(
      res => {
        this.router.navigate(['/']);
      }
    )
  }

}
